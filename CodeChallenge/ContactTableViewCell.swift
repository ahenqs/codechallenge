//
//  ContactTableViewCell.swift
//  CodeChallenge
//
//  Created by André Henrique da Silva on 10/12/15.
//  Copyright © 2015 André Henrique da Silva. All rights reserved.
//

import UIKit

public class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!

    override public func awakeFromNib() {
        super.awakeFromNib()

        //Removes space before cell separator
        self.separatorInset = UIEdgeInsetsZero
        self.layoutMargins = UIEdgeInsetsZero
    }

    override public func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(contact: Contact){
        self.nameLabel?.text = contact.valueForKey("name") as? String
        self.emailLabel?.text = contact.valueForKey("email") as? String
        
        if let data = contact.valueForKey("photo") as? NSData {
            
            self.photoImageView?.contentMode = UIViewContentMode.ScaleAspectFill
            self.photoImageView?.image = UIImage(data: data)
            
            self.photoImageView.layer.cornerRadius = 30.0
        } else {
            self.photoImageView.image = nil
        }
    }

}
