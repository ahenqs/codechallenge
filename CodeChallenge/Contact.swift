//
//  Person.swift
//  CodeChallenge
//
//  Created by André Henrique da Silva on 10/12/15.
//  Copyright © 2015 André Henrique da Silva. All rights reserved.
//

import Foundation
import CoreData

public class Contact: NSManagedObject {
    
    @NSManaged public var name:  String
    @NSManaged public var email: String
    @NSManaged var born:  NSDate
    @NSManaged var bio:   String
    @NSManaged var photo: NSData
    
}
