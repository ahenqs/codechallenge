//
//  DBHelper.swift
//  CodeChallenge
//
//  Created by André Henrique da Silva on 10/12/15.
//  Copyright © 2015 André Henrique da Silva. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

public class DBHelper: NSObject {
    
    public var delegate: DBHelperDelegate?
    public var total = 0
    public var totalUpdated = 0
    
    public var managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    //saves a contact
    func save(contact: Contact) -> NSError? {
        
        let managedContext = contact.managedObjectContext
        
        do {
            try managedContext!.save()
            return nil
            
        } catch let error as NSError {
            return error
        }
    }
    
    //lists all contacts
    func allContacts() -> [Contact] {
        
        let fetchRequest = NSFetchRequest(entityName: "Contact")
        
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            
            let results = try self.managedObjectContext.executeFetchRequest(fetchRequest)
            
            return results as! [Contact]
            
        } catch {
            return []
        }
        
    }
    
    //saves all downloaded contacts
    func updateWithData(data: JSON, controller: UIViewController){
        
        self.total = data.count
        self.totalUpdated = 0
        
        let entity = NSEntityDescription.entityForName("Contact", inManagedObjectContext: self.managedObjectContext)
        
        for (_,dict):(String, JSON) in data {
            
            let contact = Contact(entity: entity!, insertIntoManagedObjectContext: self.managedObjectContext)
            
            contact.name = dict["name"].string!
            contact.email = dict["email"].string!
            
            if let born: String = dict["born"].string! {
                
                if let newDate: NSDate = born.simpleToDate() as NSDate? {
                    contact.born = newDate
                }
                
            }
            
            contact.bio = dict["bio"].string!
            
            let imageURL:NSURL = NSURL(string: dict["photo"].string!)!
            let request: NSURLRequest = NSURLRequest(URL: imageURL)
            let mainQueue = NSOperationQueue.mainQueue()
            
            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, odata, error) -> Void in
                if error == nil {
                    let image = UIImage(data: odata!)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        
                        do {
                            
                            if(image != UIImage() && image != nil){
                                
                                contact.photo = (UIImagePNGRepresentation(image!))!
                            }
                            
                            try self.managedObjectContext.save()
                            
                            self.totalUpdated++
                            
                            if (self.total == self.totalUpdated){
                                self.delegate?.didUpdateDatabaseSuccessfully(data, controller: controller)
                            }
                            
                        } catch let error as NSError {
                            
                            self.delegate?.didErrorOcurred(error, controller: controller)
                        }
                        
                    })
                } else {
                    print("Error: \(error!.localizedDescription)")
                    
                    self.delegate?.didErrorOcurred(error!, controller: controller)
                }
            })
            
        }
    }
    
    //remove a single contact
    func removeContact(contact: Contact) -> NSError? {
        let context = contact.managedObjectContext
        context!.deleteObject(contact)
        
        do {
            try context!.save()
            return nil
            
        } catch let error as NSError {
            return error
        }
    }
    
    //removes all contacts from database
    func removelAllContacts(tableName: String, controller: UIViewController){
        
        let fetchRequest = NSFetchRequest()
        fetchRequest.entity = NSEntityDescription.entityForName(tableName, inManagedObjectContext: self.managedObjectContext)
        fetchRequest.includesPropertyValues = false
        
        let error:NSError?
        
        do {
            if let results = try! self.managedObjectContext.executeFetchRequest(fetchRequest) as? [NSManagedObject] {
                for result in results {
                    self.managedObjectContext.deleteObject(result)
                }
                
                try self.managedObjectContext.save()
                
                self.delegate?.didUpdateDatabaseSuccessfully(nil, controller: controller)
                
            } else {
                error = NSError(domain: "Error", code: 1, userInfo: ["localizedDescription":"Error updating data."])
                self.delegate?.didErrorOcurred(error!, controller: controller)
            }
        } catch let error as NSError {
            
            self.delegate?.didErrorOcurred(error, controller: controller)
        }
    }

}
