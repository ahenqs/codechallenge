//
//  WebService.swift
//  CodeChallenge
//
//  Created by André Henrique da Silva on 10/12/15.
//  Copyright © 2015 André Henrique da Silva. All rights reserved.
//

import Foundation
import SwiftyJSON

public protocol WebServiceDelegate {
    func connectionSucceded(data:JSON, instance:AnyObject)
    func connectionFailed(data:NSDictionary, instance:AnyObject)
}

public class WebService: NSObject, NSURLConnectionDataDelegate {
    
    let URL = "http://192.168.1.34/";
//    let URL = "http://localhost/";
    var data = NSMutableData()
    var delegate:WebServiceDelegate?
    var connection: NSURLConnection!
    
    func post(var wsName:String, wsParams:AnyObject) {
        
        if (!wsName.containsString("http://")){
            wsName = "\(URL)\(wsName)"
        }
        
        let url = NSURL(string: wsName)
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:url!)
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(wsParams, options: NSJSONWritingOptions())
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.HTTPMethod = "POST"
        
        connection = NSURLConnection(request: request, delegate: self, startImmediately: true)
    }
    
    func get(urlString:String) {
    
        let url = NSURL(string: urlString)
        
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:url!)
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        connection = NSURLConnection(request: request, delegate: self, startImmediately: true)
    
    }
    
    public func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        
        self.data = NSMutableData()
    }
    
    public func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        
        self.data.appendData(data)
        
    }
  public func connectionDidFinishLoading(connection: NSURLConnection) {
        
        if let jsonResponse:JSON = JSON(data:self.data){
            
            self.delegate?.connectionSucceded(jsonResponse, instance: self)
            
        } else {
            
            self.delegate?.connectionFailed(["error":"Could not get results."], instance: self)
        }
    }
    
    public func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        
        self.delegate?.connectionFailed(["error":error.localizedDescription], instance: self)
    }
    
}
