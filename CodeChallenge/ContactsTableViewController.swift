//
//  ContactsTableViewController.swift
//  CodeChallenge
//
//  Created by André Henrique da Silva on 10/12/15.
//  Copyright © 2015 André Henrique da Silva. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON
import ReachabilitySwift

class ContactsTableViewController: UITableViewController, ContactDelegate, WebServiceDelegate, DBHelperDelegate, TableViewDelegate {
    
    var contacts = [Contact]()
    var dataSource: TableViewDataSource?
    var selectedIndexPath: NSIndexPath?
    var wsContacts: WebService!
    var loading: Loading = Loading()
    var dbHelper = DBHelper()
    var animate = true
    var hasConnection:Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dbHelper.delegate = self

        loadInterface()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        animateTable()
        
        animate = true
    }
    
    func animateTable() {

        loadDataFromDB()
        
        if (animate){
        
            let cells = tableView.visibleCells
            let tableHeight: CGFloat = tableView.bounds.size.height
            
            for i in cells {
                let cell: UITableViewCell = i as UITableViewCell
                cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
            }
            
            var index = 0
            
            for a in cells {
                let cell: UITableViewCell = a as UITableViewCell
                UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.CurveLinear, animations: {
                    cell.transform = CGAffineTransformMakeTranslation(0, 0)
                    }, completion: nil)
                
                index++
            }
        }
    }
    
    //loads basic interface components
    func loadInterface(){
        let addButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: Selector("addNewContact:"))
        self.navigationItem.rightBarButtonItem = addButton
        
        //removes space before empty cell separators
        self.tableView.separatorInset = UIEdgeInsetsZero
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // + sign action on the right top of navigation bar
    @IBAction func addNewContact(sender: UIButton) {
        self.performSegueWithIdentifier("contact", sender: nil)
        self.animate = false
    }
    
    //shows a simple alert to the user
    func showAlert(title: String, message: String, buttonTitle: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: buttonTitle, style: .Default, handler: nil))
        
        let popOver = alertController.popoverPresentationController
        popOver?.barButtonItem = self.navigationItem.rightBarButtonItem
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.Any
        alertController.modalPresentationStyle = UIModalPresentationStyle.Popover
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - TableViewDelegate methods

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70.0
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("contact", sender: self.contacts[indexPath.row])
        self.animate = false
    }

    // MARK: Navigation methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "contact"){
            
            let editVC = segue.destinationViewController as! ContactEditViewController
            
            if (sender != nil){
                editVC.contact = sender as! Contact
            }
            
            editVC.delegate = self
            
        }
    }
    
    // MARK: Delete Contact methods
    
    //dialog before deleting a contact
    func confirmDelete(contact: Contact) {
        let alertController = UIAlertController(title: "Delete Contact", message: "Are you sure you want to permanently delete \(contact.name)?", preferredStyle: .ActionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: handleDeleteContact)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: cancelDeleteContact)
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        let popOver = alertController.popoverPresentationController
        popOver?.barButtonItem = self.navigationItem.rightBarButtonItem
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.Any
        alertController.modalPresentationStyle = UIModalPresentationStyle.Popover
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    //deleting a contact
    func handleDeleteContact(alertAction: UIAlertAction!) -> Void {
        if let indexPath = self.selectedIndexPath {
            
            let contact: Contact = self.contacts[indexPath.row]
            
            if let error = dbHelper.removeContact(contact) {
                
                showAlert("Hey!", message: "Error deleting data: \(error.userInfo)", buttonTitle: "OK")
                
            } else {
                
                loadDataFromDB()
                
                self.selectedIndexPath = nil
            }
            
        }
    }
    
    //user cancels deleting a contact
    func cancelDeleteContact(alertAction: UIAlertAction!) {
        self.selectedIndexPath = nil
    }
    
    // MARK: PersonDelegate methods
    
    //right after saving a contact in Contact Editi View Controller
    func contactDidEndEditing(contact: Contact, viewController: UIViewController) {
        saveContact(contact)
    }

    // MARK: CoreData methods
    
    //refreshes datasource for tableview
    func refreshDataSource(){
        
        self.dataSource = TableViewDataSource(items: self.contacts, cellIdentifier: "cell", configureBlock: { (cell, item) -> () in
            
            if let actualCell = cell as? ContactTableViewCell {
                if let actualContact = item as? Contact {
                    actualCell.configure(actualContact)
                }
            }
            
        })
        
        self.dataSource?.delegate = self
        
        self.tableView.dataSource = self.dataSource
    }
    
    //loads all contacts from database
    func loadDataFromDB(){
        
        self.contacts = dbHelper.allContacts()
        
        if (self.contacts.count > 0){
            
            self.navigationItem.leftBarButtonItem = self.editButtonItem()
            
            let total = self.contacts.count
            
            self.title = total == 1 ? "1 Contact" : "\(total) Contacts"
            
        } else {
            
            self.title = "Contacts"
            
            self.navigationItem.leftBarButtonItem = nil
            
            self.performSelector("confirmDownload", withObject: nil, afterDelay: 1.0)
        }
        
        refreshDataSource()
        
        self.tableView.reloadData()
    }
    
    //saves a contact to database
    func saveContact(contact: Contact){
        
        if let error = dbHelper.save(contact) {
            
            showAlert("Hey!", message: "Error saving data: \(error.userInfo)", buttonTitle: "OK")

        } else {
            
            loadDataFromDB()
        }
    }
    
    // MARK: Server methods
    
    //dialog to offer download of contacts from server
    func confirmDownload() {
        let alertController = UIAlertController(title: "Download Contacts", message: "Do you want to download contacts from server?", preferredStyle: .ActionSheet)
        
        let downloadAction = UIAlertAction(title: "Yes, download now", style: .Default, handler: handleDownloadFromServer)
        let cancelAction = UIAlertAction(title: "No, download later", style: .Cancel, handler: cancelDownloadFromServer)

        alertController.addAction(downloadAction)
        alertController.addAction(cancelAction)
        
        let popOver = alertController.popoverPresentationController
        popOver?.barButtonItem = self.navigationItem.rightBarButtonItem
        popOver?.permittedArrowDirections = UIPopoverArrowDirection.Any
        alertController.modalPresentationStyle = UIModalPresentationStyle.Popover

        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    //starts downloading contacts from server
    func handleDownloadFromServer(alertAction: UIAlertAction!) -> Void {
    
        if (Reachability.isConnectedToNetwork()){
        
            self.loading.showLoading(self.view)
            
            self.wsContacts = WebService()
            self.wsContacts.delegate = self
            
            self.wsContacts.get(Constants.kServer)
        } else {
            
            showAlert("Hey!", message: "No internet connection available. Check your settings.", buttonTitle: "OK")
            
        }
        
    }
    
    //user dimisses download dialog and gets a reminder he/she can go to settings
    func cancelDownloadFromServer(alertAction: UIAlertAction!) {
        //show message to go to Settings
        showAlert("Hey!", message: "You can always download contacts from server by clicking on\nSettings > Download from Server.", buttonTitle: "OK")
    }
    
    // MARK: WebServiceDelegate methods
    
    func connectionSucceded(data: JSON, instance: AnyObject) {
        
        self.loading.hideLoading(self.view)
        
        dbHelper.updateWithData(data, controller: self)
    }
    
    func connectionFailed(data: NSDictionary, instance: AnyObject) {
        
        self.loading.hideLoading(self.view)
        
        //there could be a message here <o/
    }
    
    // MARK: DBHelperDelegate methods
    
    func didUpdateDatabaseSuccessfully(data: JSON, controller: UIViewController) {
        self.loadDataFromDB()
    }
    
    func didErrorOcurred(error: NSError, controller: UIViewController) {        
        showAlert("Hey!", message: "Error saving data: \(error.userInfo)", buttonTitle: "OK")
    }
    
    // MARK: TableViewDelegate method
    
    func tableViewDidDeleteRow(indexPath: NSIndexPath) {
        self.selectedIndexPath = indexPath

        let contact: Contact = self.contacts[indexPath.row]

        confirmDelete(contact)
    }
}
